package wding.twittercrawler;

public class Tweet {
	
	private String tweetID;
	private String hashtag;
	private String content;
	private String timeStamp;
	private String userID;
	public String getTweetID() {
		return tweetID;
	}
	public void setTweetID(String tweetID) {
		this.tweetID = tweetID;
	}
	public String getHashtag() {
		return hashtag;
	}
	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	
	public String toString(){
		return tweetID+";"+content+";"+hashtag+";"+timeStamp;
	}
	
	
	

}
