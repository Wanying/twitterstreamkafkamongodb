package wding.twittercrawler;

import java.util.Arrays;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterKafkaProducer {
	
	public static Producer<String,Tweet> producer;
	
	public TwitterKafkaProducer(){

			
		 Properties properties = new Properties();
		 properties.put("metadata.broker.list", "broker1:9092,broker2:9092");
		 properties.put("serializer.class", "kafka.serializer.StringEncoder");
		 properties.put("partitioner.class", "example.producer.SimplePartitioner");
		 properties.put("request.required.acks", "1");
	      
	      ProducerConfig config = new ProducerConfig(properties);
	      
	      producer = new Producer<String,Tweet>(config);
	}
	
	public static void run(String topic,String[] keywords) throws InterruptedException{
		
	
		String consumerKey = TwitterSourceContent.CONSUMER_KEY;
		String consumerSecret = TwitterSourceContent.CONSUMER_KEY_SECRET;
		String accessToken = TwitterSourceContent.ACCESS_TOKEN;
		String accessTokenSecret = TwitterSourceContent.ACCESS_TOKEN_SECRET;
		final String topicName = topic;
		
		ConfigurationBuilder  cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
			.setOAuthConsumerKey(consumerKey)
			.setOAuthConsumerSecret(consumerSecret)
			.setOAuthAccessToken(accessToken)
			.setOAuthAccessTokenSecret(accessTokenSecret);
		
		TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
		StatusListener listener = new StatusListener(){
			
	         public void onStatus(Status status) {      
	            Tweet tweet = new Tweet();
	            tweet.setTweetID(String.valueOf(status.getId()));
	            tweet.setUserID(String.valueOf(status.getUser().getId()));
	            tweet.setTimeStamp(String.valueOf(status.getCreatedAt()));
	            tweet.setHashtag(Arrays.toString(status.getHashtagEntities()));
	            tweet.setContent(status.getText().replace("\n", " "));
	            KeyedMessage<String, Tweet> data = new KeyedMessage<String, Tweet>(topicName, tweet.getTweetID(), tweet); 
	            System.out.println(tweet.toString());
	            producer.send(data);
	             
	         }
	         
	         public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
	            //do nothing
	         }
	         
	         public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
	            //do nothing
	         }

	         public void onScrubGeo(long userId, long upToStatusId) {
	            //do nothing
	         }      
	         
	         public void onStallWarning(StallWarning warning) {
	            //do nothing
	         }
	         
	         public void onException(Exception ex) {
	            ex.printStackTrace();
	         }
	      };
	      twitterStream.addListener(listener);
	      FilterQuery query = new FilterQuery();
	      query.language(new String[]{"en"});
	      query.track(keywords);

	      
	      twitterStream.filter(query);
	      twitterStream.sample();
	}
	
	public static void main(String args[]) throws InterruptedException{
		PropertyConfigurator.configure("log4j.properties");
		String topic = "twitter";
		String keywords[]={"trump"};
		TwitterKafkaProducer kafka = new TwitterKafkaProducer();
		kafka.run(topic, keywords);
	}

}
